package com.markyao.controller;

import com.markyao.dao.pojo.SysUser;
import com.markyao.service.LoginService;
import com.markyao.utils.UserThreadLocal;
import com.markyao.vo.Result;
import com.markyao.vo.parmas.LoginParmas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("login")
public class LoginController {
    @Autowired
    private LoginService loginService;

    @PostMapping
    public Result login(@RequestBody LoginParmas loginParmas){
        Result login = loginService.login(loginParmas);

        return login;
    }
}
