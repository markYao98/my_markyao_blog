package com.markyao.controller;

import com.markyao.baiduapi.Sample;
import com.markyao.common.aop.LogAnnotation;
import com.markyao.common.cache.Cache;
import com.markyao.service.ArticleService;
import com.markyao.vo.parmas.ArticleParmas;
import com.markyao.vo.parmas.PageParmas;
import com.markyao.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//json数据交互
@RestController
@RequestMapping("articles")
public class ArticleController {

    @Autowired
    ArticleService articleService;

    @GetMapping("soundToText")
    public Result soundToText(){
        Sample sample=new Sample();
        String string = sample.getString();
        return Result.success(string);
    }


    @PostMapping
    @LogAnnotation(module = "文章",operation = "获取文章列表")  //代表对此接口记录日志
    public Result listArticle(@RequestBody PageParmas pageParmas){
        //首页文章列表
        return articleService.listArticle(pageParmas);
    }

    //最热文章
    @Cache(expire = 5 * 60 *1000,name = "hotArticle")
    @PostMapping("hot")
    public Result hotArticle(){
        int limit=5;
        return articleService.hotArticle(limit);
    }

    //最新文章
    @Cache(expire = 5 * 60 *1000,name = "newArticle")
    @PostMapping("new")
    public Result newArticle(){
        int limit=5;
        return articleService.newArticle(limit);
    }

    @PostMapping("listArchives")
    public Result listArchives(){

        return articleService.listArchives();
    }

    @PostMapping("view/{id}")
    public Result viewId(@PathVariable("id") Long id){
        //查看文章详情
        return articleService.findArticleById(id);
    }

    //发布文章
    @PostMapping("publish")
    public Result publish(@RequestBody ArticleParmas articleParmas){
        return articleService.publish(articleParmas);
    }

    @PostMapping("{id}")
    public Result EditById(@PathVariable("id") Long id){
        //查看文章详情
        return articleService.findArticleById(id);
    }
}
