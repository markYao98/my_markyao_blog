package com.markyao.controller;

import com.markyao.service.LoginService;
import com.markyao.vo.Result;
import com.markyao.vo.parmas.RegisterParmas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("register")
public class RegisterController {
    @Autowired
    LoginService loginService;

    @PostMapping
    public Result register(@RequestBody RegisterParmas registerParmas){
        return loginService.register(registerParmas);
    }
}
