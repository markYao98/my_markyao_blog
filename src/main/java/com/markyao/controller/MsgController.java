package com.markyao.controller;

import com.markyao.common.cache.Cache;
import com.markyao.service.MsgService;
import com.markyao.vo.Result;
import com.markyao.vo.parmas.MsgParmas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("messages")
public class MsgController {
    @Autowired
    MsgService msgService;

    @PostMapping("getqq")
    public Result getQQ(@RequestBody MsgParmas msgParmas){
        return msgService.getQQ(msgParmas.getQqNum());
    }

    @PostMapping("publish")
    public Result pulishMsg(@RequestBody MsgParmas msgParmas){
        return msgService.pulishMsg(msgParmas);
    }

    @GetMapping("getAllMsgs")
    public Result getAll(){
        return msgService.getAll();
    }

    @Cache(expire = 5 * 60 ,name = "getNewMsgs")
    @GetMapping("getNewMsgs")
    public Result getNew(){
        return msgService.getNew();
    }
}
