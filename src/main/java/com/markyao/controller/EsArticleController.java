package com.markyao.controller;

import com.markyao.es.service.ArticleEsService;
import com.markyao.vo.Result;
import com.markyao.vo.parmas.EsParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("es")
public class EsArticleController {
    @Autowired
    ArticleEsService articleEsService;

    //todo: 通过es的complate_suggest完成输入时的智能推荐搜索内容
    @PostMapping("getSuggestByPrefix")
    public Result esSuggest(@RequestBody EsParams esParams){
        return articleEsService.findBySuggest(esParams);
    }

    //todo: 通过es的fuzzy模糊查询完成智能纠错,字符乱序等模糊查询
    @PostMapping("findByQuery")
    public Result esQuery(@RequestBody EsParams esParams){

        return articleEsService.findByFuzziness(esParams);
    }
}
