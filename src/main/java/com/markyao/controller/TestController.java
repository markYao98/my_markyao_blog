package com.markyao.controller;

import com.markyao.dao.pojo.SysUser;
import com.markyao.utils.UserThreadLocal;
import com.markyao.vo.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {

    @RequestMapping
    public Result result(){
        SysUser sysUser = UserThreadLocal.get();
        UserThreadLocal.remove();
        System.out.println("-------------"+sysUser);
        return Result.success(null);
    }
// /root/mnt/docker/app
    @RequestMapping("ohYes")
    public String ohyes(){
        return "ohyes..";
    }
}
