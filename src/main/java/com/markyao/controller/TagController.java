package com.markyao.controller;

import com.markyao.dao.pojo.Tag;
import com.markyao.service.TagService;
import com.markyao.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("tags")
public class TagController {
    @Autowired
    TagService tagService;

    @PostMapping("addTag")
    public Result addTag(@RequestBody Tag tag){
        return tagService.addOneTag(tag);
    }

    @GetMapping("hot")
    public Result tagsHot(){
        int limit=6;
        return tagService.findHotTag(limit);
    }

    @GetMapping
    public Result tags(){
        //查找所有文章标签
        return tagService.findAllTags();
    }

    @GetMapping("detail")
    public Result detail(){
        //查找所有文章标签——细节
        return tagService.findAllTagsDetails();
    }

    @GetMapping("detail/{id}")
    public Result detail(@PathVariable("id") Long id){
        //查找所有文章标签——细节
        return tagService.findAllTagsDetailsById(id);
    }
}
