package com.markyao.controller;

import com.markyao.dao.pojo.Category;
import com.markyao.service.CategoryService;
import com.markyao.vo.CategoryVo;
import com.markyao.vo.Result;
import com.markyao.vo.parmas.ArticleAiParmas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("categorys")
public class CategorysController {
    @Autowired
    CategoryService categoryService;

    @PostMapping("ai")
    public Result aiCG(@RequestBody ArticleAiParmas articleAiParmas){
        return categoryService.aiCG(articleAiParmas);
    }
    @PostMapping("addCategory")
    public Result addCategory(@RequestBody Category category){
        return categoryService.addOne(category);
    }
    @GetMapping
    public Result categorys(){
        //获取所有文章类别
        return categoryService.findAllCategorys();
    }

    @GetMapping("detail")
    public Result detail(){
        //获取类别细节
        return categoryService.findAllCategorysDetails();
    }

    @GetMapping("detail/{id}")
    public Result detail(@PathVariable("id") Long id){
        //获取分类详情列表
        return categoryService.findCategorysDetailsById(id);
    }
}
