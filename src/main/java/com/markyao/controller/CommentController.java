package com.markyao.controller;

import com.markyao.service.CommentService;
import com.markyao.vo.PageResult;
import com.markyao.vo.Result;
import com.markyao.vo.parmas.CommentParmas;
import com.markyao.vo.parmas.PageParmas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("comments")
public class CommentController {
    @Autowired
    CommentService commentService;

    @GetMapping("article/{id}")
    public Result comments(@PathVariable("id") Long id){
        return commentService.findCommentByAid(id);
    }

    @PostMapping("create/change")
    public Result create(@RequestBody CommentParmas commentParmas, HttpServletRequest request)
    {
        String token = request.getHeader("Authorization");
        //发表评论，游客也可以
        return commentService.createComment(commentParmas,token);
    }


    @PostMapping("delete")
    public Result deleteComment(@RequestBody List<Long> ids,HttpServletRequest request){
        //根据评论id列表进行删除
        String token = request.getHeader("Authorization");
        return commentService.deleteByCId(ids);
    }



    @PostMapping("List")
    public Result getAllComments(@RequestBody PageParmas pageParams,HttpServletRequest request){
        //查询所有评论(分页)
        String token = request.getHeader("Authorization");
        return commentService.getAllComments(pageParams, token);
    }
}
