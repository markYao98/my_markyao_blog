package com.markyao.controller;

import com.markyao.dao.pojo.SysUser;
import com.markyao.service.SysUserService;
import com.markyao.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("users")
public class UsersController {
    @Autowired
    SysUserService sysUserService;

    @GetMapping("currentUser")
    public Result currentUser(@RequestHeader("Authorization") String token){
        return sysUserService.findUserByToken(token);
    }

    @PostMapping("update")
    public Result update(@RequestBody SysUser sysUser){
        return sysUserService.updateUser(sysUser);
    }
}
