package com.markyao.baiduapi;

import com.baidu.aip.speech.AipSpeech;
import com.markyao.utils.StrUtils;
import com.markyao.vo.Result;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;


public class Sample {

    //设置APPID/AK/SK
    public static final String APP_ID = "25986478";
    public static final String API_KEY = "23XesfBMrLO6gWNDa58V1oMF";
    public static final String SECRET_KEY = "mWrNsneGQ6MGiyD2wj8ZddZLiciMFEq6";


    public static void main(String[] args) {
        Sample sample=new Sample();
    }
    public String getString(){
        String root = System.getProperty("user.dir");
        String filename= UUID.randomUUID().toString();
        String path = root +"/"+filename+".wav";
//        File file = new File(basePath+File.separator+filename+".wav");
        File file = new File(path);
        file.setWritable(true,false);
        if(!file.getParentFile().exists()){ //如果文件的目录不存在
            file.getParentFile().mkdirs(); //创建目录
            System.out.println("创建目录");
        }
        if (!file.exists()){
            try {
                file.createNewFile();//创建文件
                System.out.println("创建文件-->"+file.getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // 初始化一个AipSpeech
        AipSpeech client = new AipSpeech(APP_ID, API_KEY, SECRET_KEY);
        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);

        HashMap<String,Object>map=new HashMap<>();
        map.put("dev_pid",1537);

        EngineeCore engineeCore=new EngineeCore(filename);
        engineeCore.startRecognize();
        if (!engineeCore.success){
            return "";
        }
        // 调用接口
        JSONObject res = client.asr(filename, "wav", 16000, map);
        Object  result = res.get("result");

        return StrUtils.getFormatStr(result.toString());
    }
}
