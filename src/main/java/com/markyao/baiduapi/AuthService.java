package com.markyao.baiduapi;



import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * 获取token类
 */
public class AuthService {

    /**
     * 获取权限token
     * @return 返回示例：
     * {
     * "access_token": "24.460da4889caad24cccdb1fea17221975.2592000.1491995545.282335-1234567",
     * "expires_in": 2592000
     * }
     */
    public static void main(String[] args) {
        String auth = getAuth();
        System.out.println(auth);
        /*
        Transfer-Encoding--->[chunked]
        null--->[HTTP/1.1 200 OK]
        Server--->[Apache]
        Cache-Control--->[no-store]
        Connection--->[keep-alive]
        Vary--->[Accept-Encoding]
        Set-Cookie--->[BAIDUID=F15E2E1AB30B82D179B41EC1C2B5C585:FG=1; expires=Thu, 31-Dec-37 23:55:55 GMT; max-age=2145916555;
        path=/; domain=.baidu.com; version=1]
        P3p--->[CP=" OTI DSP COR IVA OUR IND COM "]
        Date--->[Sat, 16 Apr 2022 07:46:50 GMT]
        Content-Type--->[application/json]
        result:{
        "refresh_token":"25.e29471796647f1ec2a584a6f5049a363.315360000.1965455210.282335-25986478",
        "expires_in":2592000,
        "session_key":"9mzdXRCqYsYx6C0iptrbRgvJTMi7E1KZSN73rUVzmilyjpvH3gLIGXljdZ7fRyrL+0f+zpGw8Q7dendspXEjTuPgbOKlag==",
        "access_token":"24.fd90f7fbf5941cd3d87ae75b3aec0612.2592000.1652687210.282335-25986478",
        "scope":"audio_voice_assistant_get brain_enhanced_asr audio_tts_post brain_speech_realtime public brain_all_scope brain_asr_async wise_adapt lebo_resource_base lightservice_public hetu_basic lightcms_map_poi kaidian_kaidian ApsMisTest_Test\u6743\u9650 vis-classify_flower lpq_\u5f00\u653e cop_helloScope ApsMis_fangdi_permission smartapp_snsapi_base smartapp_mapp_dev_manage iop_autocar oauth_tp_app smartapp_smart_game_openapi oauth_sessionkey smartapp_swanid_verify smartapp_opensource_openapi smartapp_opensource_recapi fake_face_detect_\u5f00\u653eScope vis-ocr_\u865a\u62df\u4eba\u7269\u52a9\u7406 idl-video_\u865a\u62df\u4eba\u7269\u52a9\u7406 smartapp_component smartapp_search_plugin avatar_video_test b2b_tp_openapi b2b_tp_openapi_online smartapp_gov_aladin_to_xcx","session_secret":"59c103af953dd7116d3624dbafac9fb7"}
        24.fd90f7fbf5941cd3d87ae75b3aec0612.2592000.1652687210.282335-25986478



         */
    }
    public static String getAuth() {
        // 官网获取的 API Key 更新为你注册的
        String clientId = "23XesfBMrLO6gWNDa58V1oMF";
        // 官网获取的 Secret Key 更新为你注册的
        String clientSecret = "mWrNsneGQ6MGiyD2wj8ZddZLiciMFEq6";
        return getAuth(clientId, clientSecret);
    }



    /**
     * 获取API访问token
     * 该token有一定的有效期，需要自行管理，当失效时需重新获取.
     * @param ak - 百度云官网获取的 API Key
     * @param sk - 百度云官网获取的 Secret Key
     * @return assess_token 示例：
     * "24.460da4889caad24cccdb1fea17221975.2592000.1491995545.282335-1234567"
     */
    public static String getAuth(String ak, String sk) {
        // 获取token地址
        String authHost = "https://aip.baidubce.com/oauth/2.0/token?";
        String getAccessTokenUrl = authHost
                // 1. grant_type为固定参数
                + "grant_type=client_credentials"
                // 2. 官网获取的 API Key
                + "&client_id=" + ak
                // 3. 官网获取的 Secret Key
                + "&client_secret=" + sk;
        try {
            URL realUrl = new URL(getAccessTokenUrl);
            // 打开和URL之间的连接
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.err.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String result = "";
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
            /**
             * 返回结果示例
             */
            System.err.println("result:" + result);
            JSONObject jsonObject = new JSONObject(result);
            String access_token = jsonObject.getString("access_token");
            return access_token;
        } catch (Exception e) {
            System.err.printf("获取token失败！");
            e.printStackTrace(System.err);
        }
        return null;
    }

}