package com.markyao.dao.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MsgPad {

    private Integer id;// bigint(20) NOT NULL留言id主键

    private String content;//varchar(255) NULL留言内容

    private String createDate;//varchar(127) NULL留言时间

    private String authorQq;//bigint(15) NULL留言作者的qq

    private String authorAvatar;//varchar(255) NULL留言者的头像

    private String authorName;//varchar(127) NULL留言者的昵称
}
