package com.markyao.dao.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class Category implements Serializable {
    private Long id;

    private String avatar;

    private String categoryName;

    private String description;
}
