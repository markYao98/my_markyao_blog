package com.markyao.dao.pojo;

import lombok.Data;

@Data
public class AriticleBody {
    private Long id;

    private String content;

    private String content_html;

    private Long articleId;
}
