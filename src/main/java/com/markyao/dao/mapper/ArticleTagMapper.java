package com.markyao.dao.mapper;

import com.markyao.dao.pojo.ArticleTag;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ArticleTagMapper {
    void insert(ArticleTag articleTag);

    List<Long> findAidsByTid(Long tagId);
}
