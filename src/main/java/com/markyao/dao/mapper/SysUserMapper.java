package com.markyao.dao.mapper;

import com.markyao.dao.pojo.SysUser;
import com.markyao.vo.UserVo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserMapper {

    public SysUser findSysUserByArticleId(Long artcleId);

    SysUser findAcctAndPwd(String account, String password);

    SysUser findUserByAccount(String account);

    void insertOne(SysUser sysUser);

    UserVo findUserVoById(Long authorId);

    int updateUser(SysUser sysUser);
}
