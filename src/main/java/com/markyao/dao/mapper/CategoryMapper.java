package com.markyao.dao.mapper;

import com.markyao.dao.pojo.Category;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CategoryMapper {

    Category findCategoryById(Long cId);

    List<Category> findAllCategory();

    List<Category> findAllDetails();

    int insert(Category category);
}
