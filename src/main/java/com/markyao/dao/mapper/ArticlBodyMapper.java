package com.markyao.dao.mapper;

import com.markyao.dao.pojo.AriticleBody;
import com.markyao.vo.parmas.ArticleBodyParam;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ArticlBodyMapper {

    AriticleBody findBodyByBId(Long bId);

    void insert(AriticleBody body);
}
