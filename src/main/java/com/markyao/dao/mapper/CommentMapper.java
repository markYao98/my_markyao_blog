package com.markyao.dao.mapper;

import com.markyao.dao.pojo.Comment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CommentMapper {

    //通过文章id查询评论列表
    List<Comment> findCommentList(Long id);

    //通过评论Id查询子评论列表
    List<Comment> findCommentsChild(Long id);

    //插入新评论
    void insert(Comment comment);

    //查找当前评论数
    Integer findCommentCountsByAid(Long ArticleId);

    void updateCommentCounts(@Param("counts") Integer counts,@Param("newCounts") int newCounts,@Param("articleId") Long articleId);

    void deleteByCid(List<Long> idList);

    List<Comment> findAll();

    List<Comment> findAllByUid(Long id);
}
