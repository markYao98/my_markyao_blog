package com.markyao.dao.mapper;

import com.markyao.dao.pojo.SysUser;
import com.markyao.dao.pojo.Tag;
import com.markyao.vo.TagVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface TagMapper {


    public List<Tag> findTagsByArticleId(Long artcleId);


    List<Long> finHotTagIds(int limit);

    List<Tag> findHotTags(List<Long>ids);

    List<Tag> findAll();

    List<Tag> findAllDetails();

    Tag findTagDetailsById(Long id);

    int insert(Tag tag);
}
