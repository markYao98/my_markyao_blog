package com.markyao.dao.mapper;

import com.markyao.dao.dos.Archives;
import com.markyao.dao.pojo.AriticleBody;
import com.markyao.dao.pojo.Article;
import com.markyao.vo.parmas.PageParmas;
import com.markyao.vo.Result;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ArticleMapper {

    //按照权值以及创建时间排序
    List<Article> findArticlePage();

    List<Article> findHotArticle(int limit);

    List<Article> findNewArticle(int limit);

    List<Archives> listArchives();

    Article findArticleById(Long id);


    void updateViewCounts(@Param("viewCounts") int viewCounts, @Param("newCnt") int newCnt, @Param("id") Long id);

    void insertArticle(Article article);

    void update(Article article);

    List<Article> findArticleByCid(Long categoryId);

    List<Article> findArticleByTagId(@Param("Aids")List<Long> Aids);

    List<Article> findArticleByYearAndMonth(@Param("year") String year, @Param("month") String month);

    List<Article> findArticleByQuery(String query);

    Long findUidByAid(Long articleId);

    @Select("select title from ms_article where id = #{articleId}")
    String findTitleById(Long articleId);
}
