package com.markyao.dao.mapper;

import com.markyao.dao.pojo.MsgPad;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MsgMapper {

    void insert(MsgPad msgPad);

    List<MsgPad> findAll();

    List<MsgPad> findNew();
}
