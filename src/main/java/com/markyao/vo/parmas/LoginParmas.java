package com.markyao.vo.parmas;

import lombok.Data;

@Data
public class LoginParmas {

    private String account;

    private String password;
}
