package com.markyao.vo.parmas;

import lombok.Data;

@Data
public class RegisterParmas {
    private String account;

    private String password;

    private String nickname;
}
