package com.markyao.vo.parmas;

import lombok.Data;

@Data
public class PageParmas {
    private int page;

    private int pageSize;

    private Long categoryId;

    private Long tagId;

    private String year;

    private String month;

    private String queryString;

    public String getMonth(){
        if (this.month != null && this.month.length() ==1){
            return "0"+this.month;
        }
        return this.month;
    }
}
