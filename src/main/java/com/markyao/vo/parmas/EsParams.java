package com.markyao.vo.parmas;

import lombok.Data;

import java.io.Serializable;

@Data
public class EsParams implements Serializable {

    private String prefix;//根据前缀词给出推荐

    private Integer currentPage;//当前页码

    private Integer pageSize;//每页装填数量

    private String queryString;//根据搜索词智能搜索
}
