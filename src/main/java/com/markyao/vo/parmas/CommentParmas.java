package com.markyao.vo.parmas;

import lombok.Data;

@Data
public class CommentParmas {
    private Long articleId;

    private String content;

    private Long parent;

    private Long toUserId;
}
