package com.markyao.vo.parmas;

import lombok.Data;

import java.io.Serializable;

@Data
public class ArticleAiParmas implements Serializable {
    //用于接收文章信息 产生智能分类
    String title;//文章标题

    String content;//文章内容
}
