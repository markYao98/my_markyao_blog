package com.markyao.vo.parmas;

import com.markyao.dao.pojo.QQpojo;
import lombok.Data;

@Data
public class MsgParmas {
    private String qqNum;

    private QQpojo qqPojo;

    private String content;

}
