package com.markyao.vo.parmas;

import lombok.Data;

@Data
public class ArticleBodyParam {
    private String content;

    private String contentHtml;
}
