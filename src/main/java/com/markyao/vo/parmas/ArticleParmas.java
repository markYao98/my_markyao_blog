package com.markyao.vo.parmas;

import com.markyao.vo.CategoryVo;
import com.markyao.vo.TagVo;
import lombok.Data;

import java.util.List;

@Data
public class ArticleParmas {
    private Long id;

    private ArticleBodyParam body;

    private CategoryVo category;

    private String summary;

    private List<TagVo> tags;

    private String title;

    private String summaryImg;
}
