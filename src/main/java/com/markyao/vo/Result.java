package com.markyao.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Result {
    private Boolean success;//是否处理成功

    private int code;//响应码

    private String msg;//提示信息

    private Object data;//具体响应的数据

    private Integer totalPages;//总页码

    private Long totalSize;//总个数

    public static Result success(Object data,int total_pages){
        return new Result(true,200,"success",data,total_pages,null);
    }
    public static Result success(Object data,int total_pages,long total_size){
        return new Result(true,200,"success",data,total_pages,total_size);
    }


    public static Result success(Object data){
        return new Result(true,200,"success",data,null,null);
    }

    public static Result failed(int code,String msg){
        return new Result(false,code,msg,null,null,null);
    }
}
