package com.markyao.vo;

import lombok.Data;

@Data
public class SysUserVo {

    private String id;

    private String account;

    private String nickname;

    private String avatar;

    private String password;

    private String mobilePhoneNumber;

    private String email;
}
