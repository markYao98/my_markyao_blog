package com.markyao.vo;

import lombok.Data;

@Data
public class ArticleBodyVo {
    private String content;
}
