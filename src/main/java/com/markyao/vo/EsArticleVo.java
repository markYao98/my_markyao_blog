package com.markyao.vo;

import lombok.Data;
import org.python.antlr.ast.Str;

@Data
public class EsArticleVo {

    private String id;

    private String title;

    private String tag_name;

    private Integer weight;

    private String summary_img;

    private String summary;

    private Integer comment_counts;

    private String category_name;

    private String content;

    private Long create_date;

    private String account;


    private Integer view_counts;

}
