package com.markyao.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QQuserVo {
    //QQ对象
    private String msg;

    private Integer code;

    private HashMap<String,String> data;


}
