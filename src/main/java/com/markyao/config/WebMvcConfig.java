package com.markyao.config;

import com.markyao.handler.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor;
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //跨域配置
        registry.addMapping("/**").
                allowedOrigins("http://www.markyao.top/",
                        "http://localhost:8080","http://localhost:80",
                        "http://120.77.47.129:8080",
                        "http://120.77.47.129:80");
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //登录拦截器 拦截登录,发帖即可
        registry.addInterceptor(loginInterceptor).addPathPatterns("/articles/publish");
//                .excludePathPatterns("/test","/articles/publish");
    }
}