package com.markyao.service;

import com.markyao.dao.pojo.SysUser;
import com.markyao.vo.Result;
import com.markyao.vo.UserVo;

public interface SysUserService {
    public SysUser findSysUserByArticleId(Long artcleId);

    SysUser findUser(String account, String password);

    Result findUserByToken(String token);

    SysUser findUserByAccount(String account);

    void insertOne(SysUser sysUser);

    UserVo findUserVoById(Long authorId);

    Result updateUser( SysUser sysUser);
}
