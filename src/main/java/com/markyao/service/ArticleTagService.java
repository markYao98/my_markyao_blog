package com.markyao.service;

import com.markyao.dao.pojo.ArticleTag;

import java.util.List;

public interface ArticleTagService {

    void insert(ArticleTag articleTag);

    List<Long> findAidsByTId(Long TagId);
}
