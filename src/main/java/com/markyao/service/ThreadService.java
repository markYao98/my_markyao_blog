package com.markyao.service;

import com.markyao.dao.mapper.ArticleMapper;
import com.markyao.dao.mapper.CommentMapper;
import com.markyao.dao.pojo.Article;
import com.markyao.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class ThreadService {
    @Async("taskExecutor")
    public void updateViewCounts(ArticleMapper articleMapper, Article article) {
        int viewCounts = article.getViewCounts();
        Long id = article.getId();
        int newCnt=viewCounts+1;
        articleMapper.updateViewCounts(viewCounts,newCnt,id);
        try {
            Thread.sleep(5000);
            System.out.println("更新阅读数完成...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Async("taskExecutor")
    public void updateCommentCounts(CommentMapper commentMapper, Long articleId,Integer counts) {
        int newCounts=counts+1;
        commentMapper.updateCommentCounts(counts,newCounts,articleId);
        try {
            Thread.sleep(5000);
            System.out.println("更新评论数完成...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
