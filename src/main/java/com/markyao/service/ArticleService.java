package com.markyao.service;

import com.markyao.vo.parmas.ArticleParmas;
import com.markyao.vo.parmas.PageParmas;
import com.markyao.vo.Result;

public interface ArticleService {

    /*
    分页查询文章列表
     */
    Result listArticle(PageParmas pageParmas);

    Result hotArticle(int limit);

    Result newArticle(int limit);

    Result listArchives();

    Result findArticleById(Long id);

    Result publish(ArticleParmas articleParmas);

    Long findUidByAid(Long articleId);

    String findTitleById(Long articleId);
}
