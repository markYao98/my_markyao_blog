package com.markyao.service;

import com.markyao.dao.mapper.MsgMapper;
import com.markyao.dao.pojo.MsgPad;
import com.markyao.dao.pojo.QQpojo;
import com.markyao.utils.QQ;
import com.markyao.vo.QQuserVo;
import com.markyao.vo.Result;
import com.markyao.vo.parmas.MsgParmas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MsgService {

    public Result getQQ(String qqNum) {
        //获取qq信息
        if(qqNum==null) {
            return Result.failed(-999, "请输入正确的号码");
        }
        QQ qqTools=new QQ();
        QQuserVo qqUserVo = qqTools.getQqUserVo(qqNum);
        if(qqUserVo.getCode()!=200){
            return Result.failed(-999,"获取失败!!");
        }
        QQpojo q=new QQpojo();
        q.setQqAvatar(qqUserVo.getData().get("avatar"));
        q.setQqName(qqUserVo.getData().get("name"));
        q.setQqNum(qqUserVo.getData().get("qq"));
        return Result.success(q);
    }
    @Autowired
    MsgMapper msgMapper;
    public Result pulishMsg(MsgParmas msgParmas) {
        if (msgParmas.getQqPojo().getQqNum()==null){
            return Result.failed(-999,"还未获取qq信息哦");
        }

        MsgPad msgPad=new MsgPad();
        msgPad.setAuthorQq(msgParmas.getQqPojo().getQqNum());
        msgPad.setAuthorAvatar(msgParmas.getQqPojo().getQqAvatar());
        msgPad.setAuthorName(msgParmas.getQqPojo().getQqName());
        msgPad.setContent(msgParmas.getContent());

        Date date=new Date();
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = spf.format(date);
        msgPad.setCreateDate(format);
        msgMapper.insert(msgPad);

        return Result.success(null);
    }

    public Result getAll() {
        List<MsgPad>msgPadList=new ArrayList<>();
        msgPadList=msgMapper.findAll();
        return Result.success(msgPadList);
    }

    public Result getNew() {
        List<MsgPad>msgPadList=new ArrayList<>();
        msgPadList=msgMapper.findNew();
        return Result.success(msgPadList);
    }
}
