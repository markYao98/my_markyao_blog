package com.markyao.service;

import com.markyao.dao.pojo.SysUser;
import com.markyao.dao.pojo.Tag;
import com.markyao.vo.Result;
import com.markyao.vo.TagVo;

import java.util.List;

public interface TagService {

    List<TagVo>findTahsByArticleId(Long articleId);


    Result findHotTag(int limit);

    Result findAllTags();

    Result findAllTagsDetails();

    Result findAllTagsDetailsById(Long id);

    Result addOneTag(Tag tag);
}
