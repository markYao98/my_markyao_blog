package com.markyao.service;

import com.markyao.vo.Result;
import com.markyao.vo.parmas.CommentParmas;
import com.markyao.vo.parmas.PageParmas;

import java.util.List;

public interface CommentService {

    Result findCommentByAid(Long id);

    Result createComment(CommentParmas commentParmas, String token);

    Result deleteByCId(List<Long> ids);


    Result getAllComments(PageParmas pageParams, String token);
}
