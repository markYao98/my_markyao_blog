package com.markyao.service;

import com.markyao.dao.pojo.Category;
import com.markyao.vo.CategoryVo;
import com.markyao.vo.Result;
import com.markyao.vo.parmas.ArticleAiParmas;

public interface CategoryService {
    public CategoryVo findCategoryVoById(Long cId);

    Result findAllCategorys();

    Result findAllCategorysDetails();

    Result findCategorysDetailsById(Long id);

    Result addOne(Category category);

    Result aiCG(ArticleAiParmas articleAiParmas);
}
