package com.markyao.service;

import com.markyao.dao.pojo.SysUser;
import com.markyao.vo.Result;
import com.markyao.vo.parmas.LoginParmas;
import com.markyao.vo.parmas.RegisterParmas;

public interface LoginService {

    Result login(LoginParmas loginParmas);

    SysUser checkT(String token);

    Result logout(String token);

    Result register(RegisterParmas registerParmas);
}
