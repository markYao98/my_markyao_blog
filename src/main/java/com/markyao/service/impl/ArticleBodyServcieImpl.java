package com.markyao.service.impl;

import com.markyao.dao.mapper.ArticlBodyMapper;
import com.markyao.dao.pojo.AriticleBody;
import com.markyao.service.ArticleBodyServcie;
import com.markyao.vo.parmas.ArticleBodyParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArticleBodyServcieImpl implements ArticleBodyServcie {
    @Autowired
    ArticlBodyMapper articlBodyMapper;

    @Override
    public void insert(AriticleBody body) {
        articlBodyMapper.insert(body);
    }
}
