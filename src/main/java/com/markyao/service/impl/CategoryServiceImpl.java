package com.markyao.service.impl;

import com.markyao.dao.mapper.CategoryMapper;
import com.markyao.dao.pojo.Category;
import com.markyao.service.CategoryService;
import com.markyao.utils.py.JyRest;
import com.markyao.vo.CategoryVo;
import com.markyao.vo.Result;
import com.markyao.vo.parmas.ArticleAiParmas;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryMapper categoryMapper;
    @Override
    public CategoryVo findCategoryVoById(Long cId) {
        Category category = categoryMapper.findCategoryById(cId);
        CategoryVo categoryVo=new CategoryVo();
        BeanUtils.copyProperties(category,categoryVo);
        categoryVo.setId(String.valueOf(category.getId()));
        return categoryVo;
    }

    @Override
    public Result findAllCategorys() {
        List<Category>categories=categoryMapper.findAllCategory();

        return Result.success(copyForList(categories));
    }

    @Override
    public Result findAllCategorysDetails() {
        return Result.success(categoryMapper.findAllDetails());
    }

    @Override
    public Result findCategorysDetailsById(Long id) {
        //通过id展示文章分类详情列表
        Category category = categoryMapper.findCategoryById(id);
        return Result.success(category);
    }

    @Override
    public Result addOne(Category category) {
        int cnt=categoryMapper.insert(category);
        if (cnt<=0){
            return Result.failed(-999,"添加分类失败");
        }
        return Result.success(category.getId());
    }

    @Override
    public Result aiCG(ArticleAiParmas articleAiParmas) {
        String fenlei = JyRest.getTag(articleAiParmas.getTitle(), articleAiParmas.getContent());
        if (fenlei.length()==0||"".equals(fenlei)){
            return Result.failed(-999,"响应超时错误");
        }
        return Result.success(fenlei);
    }

    private List<CategoryVo> copyForList(List<Category> categories) {
        List<CategoryVo> categoryVos=new ArrayList<>();
        for (Category category : categories) {
            categoryVos.add(copy(category));
        }
        return categoryVos;
    }

    private CategoryVo copy(Category category) {
        CategoryVo categoryVo=new CategoryVo();

        BeanUtils.copyProperties(category,categoryVo);
        categoryVo.setId(String.valueOf(category.getId()));
        return categoryVo;
    }
}
