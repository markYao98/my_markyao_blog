package com.markyao.service.impl;

import com.markyao.dao.mapper.TagMapper;
import com.markyao.dao.pojo.SysUser;
import com.markyao.dao.pojo.Tag;
import com.markyao.service.TagService;
import com.markyao.vo.Result;
import com.markyao.vo.TagVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class TagServiceImpl implements TagService {
    @Autowired
    TagMapper tagMapper;

    @Override
    public List<TagVo> findTahsByArticleId(Long articleId) {
        //通过aid进行多表查询，查询所拥有的tag标签
        List<Tag> tags = tagMapper.findTagsByArticleId(articleId);
        List<TagVo>tagVoList=copys(tags);
        return tagVoList;
    }

    @Override
    public Result findHotTag(int limit) {
        List<Long> ids = tagMapper.finHotTagIds(limit);
        if(ids.size()==0||ids==null){
            return Result.success(Collections.emptyList());
        }
        List<Tag> hotTags = tagMapper.findHotTags(ids);

        return Result.success(hotTags);
    }

    @Override
    public Result findAllTags() {
        //查找所有tag 不包括图标 只有id,类名两个字段
        List<Tag> tags=tagMapper.findAll();
        return Result.success(copys(tags));
    }

    @Override
    public Result findAllTagsDetails() {
        //查找所有细节tag(包括图标)
        return Result.success(tagMapper.findAllDetails());
    }

    @Override
    public Result findAllTagsDetailsById(Long id) {
        //通过标签id查找
        Tag tag=tagMapper.findTagDetailsById(id);
        return Result.success(copyTag(tag));
    }

    @Override
    public Result addOneTag(Tag tag) {
        int cnt=tagMapper.insert(tag);
        if (cnt==0){
            return Result.failed(-999,"添加标签失败");
        }
        return Result.success(tag.getId());
    }


    private List<TagVo> copys(List<Tag> tags) {
        List<TagVo>tagVoList=new ArrayList<>();
        for (Tag tag : tags) {
            TagVo copy = copyTag(tag);
            tagVoList.add(copy);
        }
        return tagVoList;
    }
    private TagVo copyTag(Tag tags) {
        TagVo tagVo=new TagVo();
        BeanUtils.copyProperties(tags,tagVo);
        tagVo.setId(String.valueOf(tags.getId()));
        return tagVo;
    }


}
