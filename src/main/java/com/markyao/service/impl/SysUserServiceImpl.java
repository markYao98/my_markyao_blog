package com.markyao.service.impl;

import com.alibaba.fastjson.JSON;
import com.markyao.dao.mapper.SysUserMapper;
import com.markyao.dao.pojo.SysUser;
import com.markyao.service.LoginService;
import com.markyao.service.SysUserService;
import com.markyao.vo.ErrorCode;
import com.markyao.vo.Result;
import com.markyao.vo.SysUserVo;
import com.markyao.vo.UserVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

@Service
public class SysUserServiceImpl implements SysUserService {
    @Autowired
    SysUserMapper sysUserMapper;
    @Autowired
    LoginService loginService;
    @Override
    public SysUser findSysUserByArticleId(Long artcleId) {
        //通过文章id查找作者信息
        SysUser sysUserByArticleId = sysUserMapper.findSysUserByArticleId(artcleId);
        if(sysUserByArticleId==null){
            sysUserByArticleId=new SysUser();
            sysUserByArticleId.setNickname("markYao");
        }
        return sysUserByArticleId;
    }

    @Override
    public SysUser findUser(String account, String password) {
        SysUser sysUser=sysUserMapper.findAcctAndPwd(account,password);
        return sysUser;
    }

    @Override
    public Result findUserByToken(String token) {
        /*
        1.token合法校验
        是否为空，解析是否成功，redis是否存在
        2.如果校验失败，返回错误
        3.如果成功，返回对应的结果，loginUserVo
         */
        if(StringUtils.isBlank(token)){
            return Result.failed(ErrorCode.TOKEN_ERROR.getCode(), ErrorCode.TOKEN_ERROR.getMsg());
        }
        SysUser sysUser = loginService.checkT(token);
        if(sysUser==null){
            return Result.failed(ErrorCode.TOKEN_ERROR.getCode(), ErrorCode.TOKEN_ERROR.getMsg());
        }
        SysUserVo sysUserVo=new SysUserVo();
        sysUserVo.setId(String.valueOf(sysUser.getId()));
        sysUserVo.setAccount(sysUser.getAccount());
        sysUserVo.setNickname(sysUser.getNickname());
        sysUserVo.setAvatar(sysUser.getAvatar());
        sysUserVo.setEmail(sysUser.getEmail());
        sysUserVo.setMobilePhoneNumber(sysUser.getMobilePhoneNumber());
        return Result.success(sysUserVo);
    }

    @Override
    public SysUser findUserByAccount(String account) {
       return sysUserMapper.findUserByAccount(account);
    }

    @Override
    public void insertOne(SysUser sysUser) {
        sysUserMapper.insertOne(sysUser);
    }

    @Override
    public UserVo findUserVoById(Long authorId) {
        return sysUserMapper.findUserVoById(authorId);
    }


    @Override
    public Result updateUser(SysUser sysUser) {
        String account = sysUser.getAccount();
        SysUser userByAccount = sysUserMapper.findUserByAccount(account);
        if (userByAccount!=null){
            if (!userByAccount.getId().equals(sysUser.getId()))//不是同一个用户
            return Result.failed(-888,"该账户已存在");
        }
        int cnt=sysUserMapper.updateUser(sysUser);
        if(cnt<=0){
            return Result.failed(-999,"更新失败!");
        }

        return Result.success(null);
    }
}
