package com.markyao.service.impl;

import com.markyao.dao.mapper.ArticleTagMapper;
import com.markyao.dao.pojo.ArticleTag;
import com.markyao.service.ArticleTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleTagServiceImpl implements ArticleTagService {
    @Autowired
    ArticleTagMapper articleTagMapper;

    @Override
    public void insert(ArticleTag articleTag) {
        articleTagMapper.insert(articleTag);
    }

    @Override
    public List<Long> findAidsByTId(Long TagId) {
        return articleTagMapper.findAidsByTid(TagId);
    }


}
