package com.markyao.es.repository;

import com.markyao.es.Article_model_es;
import com.markyao.es.EsResult;
import com.markyao.vo.Result;
import com.markyao.vo.parmas.EsParams;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleEsRepository {

    EsResult findAllEmployeeDetailsFromES(int currentPage);

    EsResult findByFuzziness(EsParams esParams);

    List<String> findBySuggest(String name);
//        String findBySuggest(String name);
}
