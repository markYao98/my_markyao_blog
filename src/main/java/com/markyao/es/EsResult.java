package com.markyao.es;

import lombok.Data;

import java.util.List;

@Data
public class EsResult {
    Integer totalSize;

    List<Article_model_es> list;
}
