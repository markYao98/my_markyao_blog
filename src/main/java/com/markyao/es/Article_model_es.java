package com.markyao.es;

import lombok.Data;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;

import java.util.Map;

@Data
public class Article_model_es {
    //es索引的模板类

    private Long  id;

    private String title;

    private String tag_name;

    private Integer weight;

    private String summary_img;

    private String summary;

    private Integer comment_counts;

    private String category_name;

    private String content;

    private Long create_date;

    private String account;


    private Integer view_counts;

    //高亮字段
//    private Map<String, HighlightField> highlightFields;
}
