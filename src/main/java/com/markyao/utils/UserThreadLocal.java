package com.markyao.utils;

import com.markyao.dao.pojo.SysUser;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserThreadLocal {
    private UserThreadLocal(){}

    //线程变量隔离
    private static final ThreadLocal<SysUser> LOCAL= new ThreadLocal<>();

    public static void put(SysUser sysUser){
        log.info("现在放入用户..");
        LOCAL.set(sysUser);
    }

    public static SysUser get(){
        log.info("现在获取用户..");
        return LOCAL.get();
    }

    public static void remove(){
        log.info("现在释放掉用户..");
        LOCAL.remove();
    }
}
