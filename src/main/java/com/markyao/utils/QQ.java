package com.markyao.utils;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.markyao.vo.QQuserVo;
import sun.net.www.http.HttpClient;

public class QQ {
    private static String qq_uri="https://api.usuuu.com/qq/";
    private String qq;
    private QQuserVo qqUserVo;
    public static String getQq_uri() {
        return qq_uri;
    }

    public static void setQq_uri(String qq_uri) {
        QQ.qq_uri = qq_uri;
    }


    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public QQuserVo getQqUserVo(String qq) {
        //获取qq信息
        String s = new PoolingHttpClient().doGetHtml(qq_uri + qq);
        QQuserVo qqUserVo = JSON.parseObject(s, QQuserVo.class);
        return qqUserVo;
    }

    public void setQqUserVo(QQuserVo qqUserVo) {
        this.qqUserVo = qqUserVo;
    }

    public static void main(String[] args) {
//        qq="814712640";
        String s = new PoolingHttpClient().doGetHtml(qq_uri + "814712640");
        QQuserVo jsonObject = JSON.parseObject(s, QQuserVo.class);
        System.out.println(jsonObject.getData().get("qq"));
        System.out.println(jsonObject.getData().get("name"));
        System.out.println(jsonObject.getData().get("avatar"));
    }
}
