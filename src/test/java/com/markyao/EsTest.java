package com.markyao;

import com.markyao.es.Article_model_es;
import com.markyao.es.service.ArticleEsService;
import com.markyao.vo.EsArticleVo;
import com.markyao.vo.Result;
import com.markyao.vo.parmas.EsParams;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SpringBootTest
public class EsTest {

    @Autowired
    ArticleEsService articleEsService;


    @Test
    public void test1(){
        //所有数据查询测试通过
        List<Article_model_es> allEmployeeDetailsFromES = (List<Article_model_es>) articleEsService.findAllES(2).getList();
        for (Article_model_es allEmployeeDetailsFromE : allEmployeeDetailsFromES) {
            System.out.println(allEmployeeDetailsFromE);
        }
        System.out.println("size==>"+allEmployeeDetailsFromES.size());
    }

    @Test
    public void  test2(){
        //模糊查询测试通过
        EsParams esParams=new EsParams();
        esParams.setCurrentPage(0);
        esParams.setPageSize(5);
        esParams.setQueryString("100");
        Result byFuzziness = articleEsService.findByFuzziness(esParams);
        List<EsArticleVo> data = (List<EsArticleVo>) byFuzziness.getData();
        for (EsArticleVo datum : data) {
            System.out.println(datum);
        }

    }

    @Test
    public void test3(){
        EsParams esParams=new EsParams();
        esParams.setPrefix("博客");
        Result bySuggest = articleEsService.findBySuggest(esParams);
        List<String> data = (List<String>) bySuggest.getData();
        for (String datum : data) {
            System.out.println(datum);
        }
    }

}
