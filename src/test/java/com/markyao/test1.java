package com.markyao;

import com.markyao.dao.mapper.ArticleMapper;
import com.markyao.dao.mapper.CommentMapper;
import com.markyao.dao.mapper.TagMapper;
import com.markyao.dao.pojo.Article;
import com.markyao.dao.pojo.Comment;
import com.markyao.dao.pojo.Tag;
import com.markyao.service.MsgService;
import com.markyao.utils.py.JyRest;
import com.markyao.vo.Result;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@SpringBootTest
public class test1 {
    @Autowired
    ArticleMapper articleMapper;
    @Autowired
    TagMapper tagMapper;
    @Autowired
    RedisTemplate<String,String> redisTemplate;
    @Autowired
    CommentMapper commentMapper;
    @Test
    public void test1(){
        List<Comment> all = commentMapper.findAllByUid(1l);
        for (Comment comment : all) {
            System.out.println(comment);
        }
//        List<Article> articlePage = articleMapper.findArticlePage();
//        for (Article article : articlePage) {
//            System.out.println("----------------"+article);
//        }

//        List<Tag> tags = tagMapper.finHotTag(2);
//        for (Tag tag : tags) {
//            System.out.println(tag);
//        }

//        List<Long> longs = tagMapper.finHotTagIds(2);
//        List<Tag> hotTags = tagMapper.findHotTags(longs);
//        for (Tag hotTag : hotTags) {
//            System.out.println(hotTag);
//        }

//        String admin = DigestUtils.md5Hex("Shuaige233"+"123456Mky!@#$$");//f3dd8e74bfd429e3dd3aebb35640de1d
//        String boss=DigestUtils.md5Hex("ZxC32489"+"123456Mky!@#$$");
//        System.out.println(boss);

        Long datetime=1644327981117L;
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date=new Date(datetime);
        String createDate = sdf.format(date);
        System.out.println("新时间?"+createDate);
//        String a="aaaa.jpg";
//        String suffix = a.substring(a.lastIndexOf("."));
//        String s = UUID.randomUUID().toString();
//        System.out.println("前缀+后缀:-----"+s+suffix);
    }

    @Test
    public void test2(){

        redisTemplate.opsForValue().set("key1","hello");
    }
    @Autowired
    MsgService msgService;
    @Test
    public void test3(){
//        Result qq = msgService.getQQ("814712640");
//        System.out.println("第一步:获取qq信息"+qq.getData());

    }

//    @Autowired
//    RestTemplate restTemplate;
//    @Test
//    public void test4(){
//        String title="美面临40年来最严重通胀 美财长：降低中国商品关税“值得考虑";
//        String content="据路透社报道，美国财政部长耶伦周五（4月22日）接受彭博新闻访问时说，美国政府要尽一切所能降低通胀，并以总统拜登释放战略石油储备及制定措施解决供应链问题来说明这点。她透露，采取措施降低美国对中国商品的关税是值得考虑的，因为这么做可能为降低美国通胀带来“理想的效果,此前一天，美国副国家安全顾问辛格在布雷顿森林体系委员会举办的一场活动中也建议，降低对中国一系列非战略性商品包括自行车和服装的关税，以协助抑制通胀问题。";
//
//        JyRest jyRest=new JyRest();
//        String tag = jyRest.getTag(title, content);
//        System.out.println("获取智能分类:"+tag);
//    }

    @Test
    public void test5(){
        String a="abcdfghjk";
        a=a.substring(0,3);
        System.out.println(a);
    }
}
