```
#创建文章映射
DELETE blog_article
PUT blog_article
{
  "settings": {
    "analysis": {
      "analyzer": {
        "my_analyzer":{
          "tokenizer":"ik_max_word",
          "type":"custom",
          "filter":["lowercase"]
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "title": {
        "analyzer": "my_analyzer",
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "summary": {
        "analyzer": "my_analyzer",
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "article_body": {
        "analyzer": "my_analyzer",
        "type": "text"
      },
      "suggest": {
        "type": "completion",
        "analyzer": "my_analyzer",
        "contexts": [
          {
            "name": "category",
            "type": "category"
          },
          {
            "name": "tags",
            "type": "category"
          }
        ]
      }
    }
  }
}

#插入数据
GET blog_article/_mapping
PUT blog_article/_create/1
{
  "title": "修复了评论的bug",
  "summary": "完成修复",
  "article_body": "1.首先是mysql中评论表的article_id字段和文章表里的id应该是相对应的；然而评论表里的该字段类型是int型而文章表里id字段为bigint,这样导致了有些文章无法评论的原因就是插入评论表时文章id过长而产生了参数错误。将评论表里的文章id字段修改为bigint类型即可.ALTER TABLE ms_comment MODIFY article_id BIGINT(20)2.评论里面不显示多层次的bug也修复了.原因在于返回vo对象时后台的long类型id到了前台产生了精度损失。所以需要转Json返回时要转成string类型的即可解决。@JsonSerialize(using = ToStringSerializer.class)",
  "suggest": {
    "input": [
      "修复了评论的bug"
    ],
    "contexts": {
      "category": "bug修复",
      "tags":["springboot"]
    }
  }
}
PUT blog_article/_bulk?filter_path=item.*.error
{"index":{"_id":"1"}}
{"title":"修复了评论的bug","summary":"完成修复","article_body":"1.首先是mysql中评论表的article_id字段和文章表里的id应该是相对应的；然而评论表里的该字段类型是int型而文章表里id字段为bigint,这样导致了有些文章无法评论的原因就是插入评论表时文章id过长而产生了参数错误。将评论表里的文章id字段修改为bigint类型即可.ALTER TABLE ms_comment MODIFY article_id BIGINT(20)2.评论里面不显示多层次的bug也修复了.原因在于返回vo对象时后台的long类型id到了前台产生了精度损失。所以需要转Json返回时要转成string类型的即可解决。@JsonSerialize(using = ToStringSerializer.class)","suggest":{"input":["修复了评论的bug"],"contexts":{"category":"bug修复","tags":["springboot"]}}}
{"index":{"_id":"2"}}
{"title":"博客项目搭建","summary":"项目搭建","article_body":"项目使用技术 ：springboot + mybatis + redis + mysql、环境部署1.前端工程导入HbuilderX,编译运行2.MAVEN工程依赖:springboot相关依赖、阿里巴巴json处理、commonMD5加密、JJWT处理token3.配置后台端口配置8888、配置跨域允许——8080、mybatis配置、redis端口配置、开启允许spingBoot循环依赖","suggest":{"input":["博客项目搭建"],"contexts":{"category":"学习笔记","tags":["springboot"]}}}
{"index":{"_id":"3"}}
{"title":"JUC学习日志-01","summary":"线程，锁，生产者消费者问题","article_body":"一、什么是JUC?全称:java.util.concurrent 是java自带的一个包. 二、线程和进程一、线程有几个状态? 观察源码发现,6个状态Thread.State//Thread的枚举类NEW, 新生RUNNABLE, 运行时BLOCKED, 阻塞WAITING, 等待（死死等待）TIMED_WAITING, 超时等待（过时不候）TERMINATED; （终止）","suggest":{"input":["JUC学习日志-01"],"contexts":{"category":"学习笔记","tags":["java"]}}}
{"index":{"_id":"4"}}
{"title":"Java链表","summary":"突发奇想写JAVA链表","article_body":"在初学JAVA的时候，就听说JAVA是没有指针的。那么JAVA是真的没有指针吗？我个人的理解是有，也算没有。有的原因是JAVA处是指针，对象的引用就是典型的指针指向对象；说没有的原因是，JAVA不能做指针运算(类似int *p=a[0];p++;)说回链表其实从入了JAVA的门开始，就几乎很少用过链表了。依稀记得LinkedList是底层是链表实现，但是实际在用的时候极少用到它。都是一个ArrayList走天下。所以今天突发奇想自己用JAVA写一个链表试试。一开始还真有点无从下手,在网上看了看,得到一些启发。以前用C写链表，都是在结构体里面定义链表的结点结构。其实放到JAVA里面就是一个类。","suggest":{"input":["Java链表"],"contexts":{"category":"学习笔记","tags":["java"]}}}
{"index":{"_id":"5"}}
{"title":"谈谈CAP","summary":"CAP原理拆解、Zookeeper、Eureka","article_body":"什么是CAPCAP原则（redis mongdb）C Consistency 强一致性A Availability 可用性P Partition tolerance 分区容错性CAP往往不能兼得。三进二————CA AP CP","suggest":{"input":["谈谈CAP"],"contexts":{"category":"学习笔记","tags":["spring-cloud"]}}}
{"index":{"_id":"6"}}
{"title":"java和python那点事","summary":"java调用python","article_body":"JAVA调用python可以用jython调用（简单的）,但是涉及到python项目会各种报错,什么编码错误啊或者是缺失模块之类的。于是我想到了类似于java开发时前后端分离的方式，利用http来调用，在python端跑服务,提供一个调用的网络接口，java这边直接访问就行了。","suggest":{"input":["java和python那点事"],"contexts":{"category":"后端","tags":["java"]}}}
{"index":{"_id":"7"}}
{"title":"JAVA基础——注解","summary":"注解的使用以及应用场景","article_bod":"之前对注解都是只知其然不知所以然，今天就好好的学习一下它的原理以及使用。资料来源:菜鸟教程Java 注解（Annotation）又称 Java 标注，是 JDK5.0 引入的一种注释机制。Java 语言中的类、方法、变量、参数和包等都可以被标注。和 Javadoc 不同，Java 标注可以通过反射获取标注内容。在编译器生成类文件时，标注可以被嵌入到字节码中。Java 虚拟机可以保留标注内容，在运行时可以获取到标注内容 。 当然它也支持自定义 Java 标注。","suggest":{"input":["JAVA基础——注解"],"contexts":{"category":"学习笔记","tags":["java"]}}}
{"index":{"_id":"8"}}
{"title":"注解","summary":"注解的使用以及应用场景","article_body":"之前对注解都是只知其然不知所以然，今天就好好的学习一下它的原理以及使用。资料来源:菜鸟教程Java 注解（Annotation）又称 Java 标注，是 JDK5.0 引入的一种注释机制。Java 语言中的类、方法、变量、参数和包等都可以被标注。和 Javadoc 不同，Java 标注可以通过反射获取标注内容。在编译器生成类文件时，标注可以被嵌入到字节码中。Java 虚拟机可以保留标注内容，在运行时可以获取到标注内容 。 当然它也支持自定义 Java 标注。","suggest":{"input":["java注解"],"contexts":{"category":"学习笔记","tags":["java"]}}}


GET blog_article/_search
#模糊搜索测试
GET blog_article/_search
{
  "query": {
    "match": {
      "title": "java"
    }
  }
}
#标题查询(智能纠错)
GET blog_article/_search
{
  "query": {
    "match": {
      "title": {
        "query": "jvaa",
        "fuzziness": 2
      }
    }
  }
}
#文章查询(智能纠错)
GET blog_article/_search
{
  "query": {
    "match": {
      "article_body": {
        "query": "jaav",
        "fuzziness": "2"
      }
    }
  }
}
#多字段查询
GET blog_article/_search
{
  "query": {
    "multi_match": {
      "query": "jaav",
      "fuzziness": 1, 
      "fields": ["title","summary","article_body"]
    }
  }
}
#高亮
GET blog_article/_search
{
  "query": {
    "multi_match": {
      "query": "jaav",
      "fuzziness": 1,
      "fields": [
        "title",
        "summary",
        "article_body"
      ]
    }
  },
  "highlight": {
    "boundary_scanner_locale": "zh_CN",
    "number_of_fragments": 5, 
    "fields": {
      "title": {
        "pre_tags": [
          "<em>"
        ],
        "post_tags": [
          "</em>"
        ]
      },
      "summary": {
        "pre_tags": [
          "<em>"
        ],
        "post_tags": [
          "</em>"
        ]
      },
      "article_body": {
        "pre_tags": [
          "<em>"
        ],
        "post_tags": [
          "</em>"
        ]
      }
    }
  }
}
#前缀智能推荐测试
GET blog_article/_search?pretty
{
  
  "suggest": {
    "blog_suggestion": {
      "prefix": "java",
      "completion": {
        "field": "suggest",
        "contexts": {
          "category": [                             
            { "context": "后端" },
            { "context": "学习笔记", "boost": 2 }
          ],
          "tags":[
            {"context": "java" }
            ]
        }
      }
    }
  }
}



#创建文章映射(content_suggest,需要json类型)
PUT blog_article
{
  "settings": {
    "analysis": {
      "analyzer": {
        "my_analyzer":{
          "tokenizer":"ik_max_word",
          "type":"custom",
          "filter":["lowercase"]
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "title": {
        "analyzer": "my_analyzer",
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "summary": {
        "analyzer": "my_analyzer",
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "content": {
        "analyzer": "my_analyzer",
        "type": "text"
      },
      "comment_counts": {
        "type": "integer"
      },
      "create_date": {
        "type": "date"
      },
      "view_counts": {
        "type": "integer"
        
      },
      "weight": {
        "type": "integer"
      },
      "account": {
        "analyzer": "my_analyzer",
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "summary_img": {
        "type": "text"
      },
      
      "suggest": {
        "type": "completion",
        "analyzer": "my_analyzer",
        "contexts": [
          {
            "name": "category_name",
            "type": "category"
          },
          {
            "name": "tag_name",
            "type": "category"
          }
        ]
      }
    }
  }
}


-------------------------------------------------------------------

#comletion_suggerter版
PUT blog_article
{
  "settings": {
    "analysis": {
      "analyzer": {
        "my_analyzer":{
          "tokenizer":"ik_max_word",
          "type":"custom",
          "filter":["lowercase"]
        }
      }
    }
  }, 
  "mappings": {
    "properties": {
      "title": {
        "analyzer": "my_analyzer",
        "type": "text",
        "fields": {
          "suggest":{
            "type": "completion",
            "analyzer": "ik_max_word"
        },
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "summary": {
        "analyzer": "my_analyzer",
        "type": "text",
        "fields": {
          "suggest":{
            "type": "completion",
            "analyzer": "ik_max_word"
        },
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "content": {
        "analyzer": "my_analyzer",
        "type": "text"
      },
      "category_name": {
        "analyzer": "my_analyzer",
        "type": "text"
      },
      "tag_name": {
        "analyzer": "my_analyzer",
        "type": "text"
      },
      "comment_counts": {
        "type": "integer"
      },
      "create_date": {
        "type": "date"
      },
      "view_counts": {
        "type": "integer"
        
      },
      "weight": {
        "type": "integer"
      },
      "account": {
        "analyzer": "my_analyzer",
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "summary_img": {
        "type": "text"
      }
    }
  }
}

GET blog_article/_mapping
GET blog_article/_search?size=60
#智能前缀推荐查询(complate)
GET blog_article/_search?pretty
{
  "suggest": {
    "car_suggest": {
      "prefix": "完成",
      "completion": {
        "field": "title.suggest"
      }
    }
  }
}


#模糊搜索测试
GET blog_article/_search
{
  "query": {
    "match": {
      "title": "完成"
    }
  }
}
#标题查询(智能纠错)
GET blog_article/_search
{
  "query": {
    "match": {
      "title": {
        "query": "博古",
        "fuzziness": 2
      }
    }
  }
}
#文章查询(智能纠错)
GET blog_article/_search
{
  "query": {
    "match": {
      "article_body": {
        "query": "jaav",
        "fuzziness": "2"
      }
    }
  }
}
#多字段查询
GET blog_article/_search
{
  "query": {
    "multi_match": {
      "query": "jaav",
      "fuzziness": 1,
      "fields": [
        "title",
        "summary",
        "content"
      ]
    }
  },
  "highlight": {
    "boundary_scanner_locale": "zh_CN",
    "number_of_fragments": 5, 
    "fields": {
      "title": {
        "pre_tags": [
          。fi
        ],
        "post_tags": [
          "</em>"
        ]
      },
      "summary": {
        "pre_tags": [
          "<em>"
        ],
        "post_tags": [
          "</em>"
        ]
      },
      "content": {
        "pre_tags": [
          "<em>"
        ],
        "post_tags": [
          "</em>"
        ]
      }
    }
  }
}
```

